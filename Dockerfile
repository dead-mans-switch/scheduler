# uses the multi-stage build pattern to  1) create the binary and  2) package the binary into an image
# use maven as the builder base image
FROM maven:3.6.3-openjdk-11 AS builder

# copy pom into the builder container
COPY pom.xml pom.xml

# copy source into the container
COPY src/main src/main

# build and test the binary
RUN mvn clean verify


# use an openjdk 11 image to package the binary
FROM adoptopenjdk/openjdk11:alpine-jre

# create the directory to store the scheduler resources
RUN mkdir -p /dms-scheduler

# set the app directory as the current working directory
WORKDIR /dms-scheduler

# restrict the application permissions by creating and then running as a standard user
RUN addgroup -S dms-scheduler && adduser -S dms-scheduler -G dms-scheduler

# copy only the final fat jar from the builder container
COPY --from=builder target/scheduler-*.jar app.jar

# make the scheduler user the owner of the app directory
RUN chown -R dms-scheduler:dms-scheduler /dms-scheduler

# change to the scheduler user
USER dms-scheduler:dms-scheduler

# run the application as the default entrypoint
ENTRYPOINT ["java","-jar","app.jar"]