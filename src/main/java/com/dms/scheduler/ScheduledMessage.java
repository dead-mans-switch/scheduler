package com.dms.scheduler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduledMessage {
    private String id;
    private String email;
    private String content;

    @Override
    public String toString() {
        return "id: " + id + ", email: " + email + ", content: " + content;
    }
}
