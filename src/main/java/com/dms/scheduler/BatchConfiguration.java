package com.dms.scheduler;


import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("${gc-bucket-name}")
    private String gcBucketName;

    @Value("${gc-bucket-folder-name}")
    private String gcBucketFolderName;

    @Value("${gc-bucket-directory-prefix}")
    private String gcBucketDirectoryPrefix;

    @Value("${gc-pubsub-topic-name}")
    private String gcPubSubTopicName;

    @Value("${gc-project-id}")
    private String gcProjectId;


    @Bean
    public Storage getGcStorage(@Value("${gc-project-id}") String gcProjectId) {
        log.info("creating GC Storage service for project id: '{}'", gcProjectId);
        return StorageOptions.newBuilder().setProjectId(gcProjectId).build().getService();
    }

    @Bean
    public GoogleCloudStorageBlobItemReader<ScheduledMessage> googleCloudStorageBlobItemReader() {
        final String completeDirectoryPrefix = gcBucketFolderName + "/" + gcBucketDirectoryPrefix + "/";
        log.info("completeDirectoryPrefix: '{}'", completeDirectoryPrefix);
        return new GoogleCloudStorageBlobItemReader(completeDirectoryPrefix, gcProjectId, gcBucketName);
    }

    @Bean
    public MessageProcessor processor() {
        return new MessageProcessor();
    }

    @Bean
    public GoogleCloudPubSubPublisherItemWriter<ScheduledMessage> writer() {
        return new GoogleCloudPubSubPublisherItemWriter<ScheduledMessage>(gcProjectId, gcPubSubTopicName);
    }

    @Bean
    public Job schedulerMessagesJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("schedulerMessagesJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end()
                .build();
    }

    @Bean
    public Step step1(ItemWriter<ScheduledMessage> writer) {
        return stepBuilderFactory.get("step1")
                .<ScheduledMessage, ScheduledMessage> chunk(10)
                .reader(googleCloudStorageBlobItemReader())
                .processor(processor())
                .writer(writer)
                .build();
    }

}