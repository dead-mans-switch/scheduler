package com.dms.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class Application {
    public static void main(String[] args) throws Exception {
        log.info("starting the dms-scheduler...");
        SpringApplication.run(Application.class, args);
    }
}