package com.dms.scheduler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutureCallback;
import com.google.api.core.ApiFutures;
import com.google.api.gax.rpc.ApiException;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;

import java.io.IOException;
import java.util.List;

@Slf4j
public class GoogleCloudPubSubPublisherItemWriter<T> implements ItemWriter<ScheduledMessage> {

    private Publisher publisher;
    private ObjectMapper mapper;


    public GoogleCloudPubSubPublisherItemWriter(String projectId, String topicName) {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        try {
            publisher = Publisher.newBuilder(
                    ProjectTopicName.of(projectId, topicName))
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(List<? extends ScheduledMessage> scheduledMessages) throws Exception {

        for(ScheduledMessage scheduledMessage : scheduledMessages) {

            String scheduledMessageJson = mapper.writeValueAsString(scheduledMessage);
            log.info("json string message: {}", scheduledMessageJson);

            PubsubMessage pubsubMessage =
                    PubsubMessage.newBuilder().setData(ByteString.copyFromUtf8(scheduledMessageJson)).build();

            ApiFuture<String> messageIdFuture = publisher.publish(pubsubMessage);
            String messageId = messageIdFuture.get();
            log.info("Published message ID: '{}'", messageId);

            //TODO: no idea why this stopped working but would be nice to have
//            ApiFuture<String> future = publisher.publish(pubsubMessage);
//            ApiFutures.addCallback(
//                    future,
//                    new ApiFutureCallback<String>() {
//
//                        @Override
//                        public void onFailure(Throwable throwable) {
//                            log.error("onFailure");
//                            if (throwable instanceof ApiException) {
//                                ApiException apiException = ((ApiException) throwable);
//                                // details on the API exception
//                                log.error("error code: '{}' - retry-able? '{}'",
//                                        apiException.getStatusCode().getCode().toString(), apiException.isRetryable());
//                            }
//                            log.error("Error publishing message: '{}'", scheduledMessageJson);
//                        }
//
//                        @Override
//                        public void onSuccess(String messageId) {
//                            // Once published, returns server-assigned message ids (unique within the topic)
//                            log.info("Published message ID: '{}'", messageId);
//                        }
//                    },
//                    MoreExecutors.directExecutor());

        }

    }

}
