package com.dms.scheduler;

public interface MessageMetadataStorageService {
    MessageMetadata get(String id);
    MessageMetadata create(MessageMetadata message);
    void delete(MessageMetadata message);
}