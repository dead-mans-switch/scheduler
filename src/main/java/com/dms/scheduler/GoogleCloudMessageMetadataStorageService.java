package com.dms.scheduler;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.google.cloud.storage.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Slf4j
@Service
public class GoogleCloudMessageMetadataStorageService implements MessageMetadataStorageService {

    @Autowired
    private Storage gcStorage;

    @Value("${gc-bucket-name}")
    private String gcBucketName;

    @Value("${gc-bucket-message-id-folder-name}")
    private String gcBucketMessageIdFolderName;

    private static final String SUPPORTED_CHARSET = "UTF-8";

    private ObjectMapper mapper;


    public GoogleCloudMessageMetadataStorageService() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Override
    public MessageMetadata get(String id) {
        String messageFolderPath = getMessageIdFolderPath(id);

        Blob blob = gcStorage.get(BlobId.of(gcBucketName, messageFolderPath));
        if(blob != null) {
            try {
                String jsonMessage = new String(blob.getContent(), SUPPORTED_CHARSET);
                MessageMetadata message = MessageMetadata.fromJson(jsonMessage);
                return message;
            } catch (UnsupportedEncodingException e) {
                log.error(e.getMessage());
                throw new RuntimeException("could not process the message id: " + id);
            }
        } else {
            log.error("failed to find message metadata for message id: {}", id);
            throw new RuntimeException("could not find the message id: " + id);
        }
    }

    @Override
    public MessageMetadata create(MessageMetadata message) {
        String messageObjectName = getMessageIdFolderPath(message);

        //write the message metadata to google cloud storage bucket
        BlobId blobId = BlobId.of(gcBucketName, messageObjectName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

        String messageMetadataContent = null;
        try {
            messageMetadataContent = mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        try {
            Blob blob = gcStorage.create(blobInfo, messageMetadataContent.getBytes(SUPPORTED_CHARSET));
            String verifyContent = new String(blob.getContent(), SUPPORTED_CHARSET);
            assert (verifyContent.contentEquals(messageMetadataContent));
        } catch (StorageException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("saved message: '{}'", message.toString());

        return new MessageMetadata(message.getId(), messageObjectName, message.getEmail(), message.getScheduledDelivery());
    }

    @Override
    public void delete(MessageMetadata message) {
        gcStorage.delete(gcBucketName, getMessageIdFolderPath(message));
    }

    private String getMessageIdFolderPath(MessageMetadata message) {
        return getMessageIdFolderPath(message.getId());
    }

    private String getMessageIdFolderPath(String id) {
        return gcBucketMessageIdFolderName + "/" + id;
    }

}
