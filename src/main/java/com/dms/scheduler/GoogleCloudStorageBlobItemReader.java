package com.dms.scheduler;

import com.google.api.gax.paging.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.google.cloud.storage.*;

import java.util.Iterator;


@Slf4j
public class GoogleCloudStorageBlobItemReader<T> implements ItemReader<ScheduledMessage> {

    private Iterator<Blob> blobIterator;

    public GoogleCloudStorageBlobItemReader(String directoryPrefix, String projectId, String bucketName) {
        log.info("searching in directory: {}", directoryPrefix);

        //TODO: use autowired bean if possible
        Storage storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();
        Bucket bucket = storage.get(bucketName);

        Page<Blob> blobs =
                bucket.list(
                        Storage.BlobListOption.prefix(directoryPrefix),
                        Storage.BlobListOption.currentDirectory());

        for (Blob blob : blobs.iterateAll()) {
            log.info("blob name: {}", blob.getName());
        }

        this.blobIterator = blobs.getValues().iterator();
    }

    @Override
    public ScheduledMessage read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if(!blobIterator.hasNext()) {
            return null;
        }
        Blob blob = blobIterator.next();
        String content = new String(blob.getContent(), "UTF-8");
        String id = blob.getName().substring(blob.getName().lastIndexOf("/") + 1);

        ScheduledMessage scheduledMessage = new ScheduledMessage();
        scheduledMessage.setId(id);
        scheduledMessage.setContent(content);

        log.info("getting a scheduledMessage: '{}'", scheduledMessage);

        return scheduledMessage;
    }
}
