package com.dms.scheduler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import com.jayway.jsonpath.JsonPath;

//TODO: separate model classes into a new project and reference both here in scheduler and api
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MessageMetadata {
    private String id;
    private String contentLocation;
    private String email;
    private LocalDateTime scheduledDelivery;

    /**
     * Parses a json representation of a message into a Message object and returns the Message object
     * @param json
     * @return
     */
    public static MessageMetadata fromJson(String json) {
        MessageMetadata message = new MessageMetadata();
        message.setId(JsonPath.read(json, "$.id"));
        message.setContentLocation(JsonPath.read(json, "$.contentLocation"));
        message.setEmail(JsonPath.read(json, "$.email"));
        message.setScheduledDelivery(LocalDateTime.parse(JsonPath.read(json, "$.scheduledDelivery")));
        return message;
    }
}
