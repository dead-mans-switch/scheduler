package com.dms.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;


@Slf4j
public class MessageProcessor implements ItemProcessor<ScheduledMessage, ScheduledMessage> {

    @Autowired
    private MessageMetadataStorageService messageMetadataStorageService;


    @Override
    public ScheduledMessage process(final ScheduledMessage scheduledMessage) {
        MessageMetadata messageMetadata = messageMetadataStorageService.get(scheduledMessage.getId());

        log.info("message Metadata found for delivery time: {} and email: {}",
                messageMetadata.getScheduledDelivery(), messageMetadata.getEmail());

        final ScheduledMessage transformedScheduledMessage = new ScheduledMessage(
                scheduledMessage.getId(), messageMetadata.getEmail(), scheduledMessage.getContent()
        );

        return transformedScheduledMessage;
    }

}