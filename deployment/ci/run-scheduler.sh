#!/bin/bash

# example: 2020/7/12/20/32
DATE_TIME=$(date -d '1 minute ago' +"%Y/%-m/%-d/%-H/%-M")
DATE_TIME_STR=$(echo "${DATE_TIME////$'-'}" )

# update image locally
docker pull us.gcr.io/dgflagg-dms-api/dms-scheduler:latest

# run the scheduler as a docker container
docker run -d --name "dms-scheduler-${DATE_TIME_STR}" \
    -e GOOGLE_APPLICATION_CREDENTIALS=/dms-scheduler/secrets/dgflagg-dms-scheduler.json\
    -e "gc-bucket-directory-prefix=${DATE_TIME}" \
    -v "$(pwd):/dms-scheduler/secrets" us.gcr.io/dgflagg-dms-api/dms-scheduler:latest