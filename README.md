Schedules messages by sending to a queue from storage bucket bin.

# build docker image locally (no build cache)
docker build -t dms-scheduler:local .

# run container locally
docker run --rm \
    -e GOOGLE_APPLICATION_CREDENTIALS=/dms-scheduler/secrets/dgflagg-dms-scheduler.json \
    -e "gc-bucket-directory-prefix=2020/7/12/20" \
    -v "$(pwd):/dms-scheduler/secrets" dms-scheduler:local

//TODO:
- unit tests
- verify messages are present in topic

# crontab schedule
*/1 * * * * ./run-scheduler.sh >> dms-dispatcher.log